#!/usr/bin/env python3

import os
import uuid
from flask import Flask, make_response, redirect, request
from werkzeug.serving import WSGIRequestHandler
from werkzeug.wrappers.response import Response as Response_t

from . import tetris_game

WSGIRequestHandler.protocol_version = "HTTP/1.1"

app = Flask(__name__)
app.secret_key = os.urandom(26)


GAMES: dict[str, tetris_game.Tetris] = dict()


@app.route('/')
def homepage() -> Response_t:
    sess_id = str(uuid.uuid4())
    return redirect(f"/new/{sess_id}", 307)


@app.route('/game-over/<uid>')
def game_over(uid: str) -> str | Response_t:
    global GAMES
    sess_id = request.cookies.get("tr_sess_id", "cookie-missing")
    game = GAMES.pop(sess_id, None)
    if game:
        return game.display_state("GAME OVER")
    else:
        sess_id = str(uuid.uuid4())
        return redirect(f"/new/{sess_id}", 307)


@app.route('/new/<sess_id>')
def new_game(sess_id: str) -> Response_t:
    content = '\033[2J\033[H'
    print(os.getcwd())
    with open(f"{__name__}/tetris-logo.txt", "r") as f:
        content += f.read()
        content += """\n\nPress any key to start...

Quick play instructions:

 - a : move piece left
 - d : move piece right
 - w : rotate piece counter clockwise
 - s : rotate piece clockwise
 - e : just move the piece downwards as is
 - f : Move the piece all the way down
"""
    global GAMES
    response = make_response(content)
    response.set_cookie("tr_sess_id", sess_id)
    GAMES[sess_id] = tetris_game.Tetris()
    return response


@app.route('/<action>')
def handle_cmd(action: str) -> str | Response_t:
    global GAMES

    sess_id = request.cookies.get("tr_sess_id", "missing-cookie")
    game = GAMES.get(sess_id)
    if not game:
        sess_id = str(uuid.uuid4())
        return redirect(f"/new/{sess_id}", 307)

    try:
        cmd = tetris_game.Tetris.Action(action)
    except ValueError:
        return game.display_state("Invalid Action")
    state = game.move(cmd)

    if state == tetris_game.Tetris.State.GameOver:
        return redirect(f"/game-over/{sess_id}", 307)

    return game.display_state(str(state))
