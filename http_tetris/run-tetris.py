#!/usr/bin/env python

import http_tetris

if __name__ == '__main__':
    http_tetris.app.run(host="0.0.0.0", port=4378)
