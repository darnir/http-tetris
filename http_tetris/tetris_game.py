#!/usr/bin/env python3

import random
from copy import deepcopy
from enum import Enum, StrEnum
from typing import NewType


class Tetris:

    PIECES = [

        [[1], [1], [1], [1]],

        [[1, 0],
         [1, 0],
         [1, 1]],

        [[0, 1],
         [0, 1],
         [1, 1]],

        [[0, 1],
         [1, 1],
         [1, 0]],

        [[1, 1],
         [1, 1]]

    ]

    CLRSCR = '\033[2J\033[H'

    INSTRUCTIONS = """Keybindings:

 - a : move piece left
 - d : move piece right
 - w : rotate piece counter clockwise
 - s : rotate piece clockwise
 - e : just move the piece downwards as is
 - f : Move the piece all the way down
 - q : Quit game"""

    class Action(Enum):
        MOVE_LEFT = 'a'
        MOVE_RIGHT = 'd'
        ROTATE_ANTICLOCKWISE = 'w'
        ROTATE_CLOCKWISE = 's'
        MOVE_DOWN = 'e'
        RUSH_DOWN = 'f'
        QUIT_GAME = 'q'
    Actions = NewType('Actions', Action)

    class State(StrEnum):
        Ok = ""
        LeftBlocked = "Cannot Move Left"
        RightBlocked = "Cannot Move Right"
        DownBlocked = "Cannot move Down"
        RotateBlocked = "Cannot rotate piece"
        GameOver = "GAME OVER"
    States = NewType('States', State)

    def __init__(self, board_size: int = 20) -> None:
        self.BOARD_SIZE = board_size
        # Extra two are for the walls, playing area will have size as BOARD_SIZE
        self.EFF_BOARD_SIZE = self.BOARD_SIZE + 2
        self.board = self.init_board()
        self.curr_piece = self.get_random_piece()
        self.piece_pos: tuple[int, int] = self.get_random_position()

    def move(self, action: Action) -> State:
        # If the piece cannot move down and the position is still the first row
        # of the board then the game has ended
        if not self.can_move_down() and self.piece_pos[0] == 0:
            return Tetris.State.GameOver

        ret = Tetris.State.Ok
        if action == Tetris.Action.MOVE_LEFT:
            ret = self.move_left()
        elif action == Tetris.Action.MOVE_RIGHT:
            ret = self.move_right()
        elif action == Tetris.Action.MOVE_DOWN:
            pass
        elif action == Tetris.Action.RUSH_DOWN:
            while self.can_move_down():
                self.move_down()
        elif action == Tetris.Action.ROTATE_ANTICLOCKWISE:
            ret = self.rotate_anticlockwise()
        elif action == Tetris.Action.ROTATE_CLOCKWISE:
            ret = self.rotate_clockwise()
        elif action == Tetris.Action.QUIT_GAME:
            ret = Tetris.State.GameOver

        if ret != Tetris.State.Ok:
            return ret

        if self.move_down() != Tetris.State.Ok:
            self.merge_board_and_piece()
            self.curr_piece = self.get_random_piece()
            self.piece_pos = self.get_random_position()
        return ret

    def overlap_check(self, new_piece: list[list[int]], new_pos: tuple[int, int]) -> bool:
        curr_piece_size_x = len(new_piece)
        curr_piece_size_y = len(new_piece[0])
        for i in range(curr_piece_size_x):
            for j in range(curr_piece_size_y):
                if self.board[new_pos[0] + i][new_pos[1] + j] == 1 and new_piece[i][j] == 1:
                    return False
        return True

    def get_clockwise_rotation(self, piece: list[list[int]]) -> list[list[int]]:
        piece_copy = deepcopy(piece)
        reverse_piece = piece_copy[::-1]
        rotated_piece = list(list(elem) for elem in zip(*reverse_piece))
        return rotated_piece

    def rotate_clockwise(self) -> State:
        rotated_piece = self.get_clockwise_rotation(self.curr_piece)
        if self.overlap_check(rotated_piece, self.piece_pos):
            self.curr_piece = rotated_piece
        else:
            return Tetris.State.RotateBlocked
        return Tetris.State.Ok

    def rotate_anticlockwise(self) -> State:
        piece_1 = self.get_clockwise_rotation(self.curr_piece)
        piece_2 = self.get_clockwise_rotation(piece_1)
        piece_3 = self.get_clockwise_rotation(piece_2)
        if self.overlap_check(piece_3, self.piece_pos):
            self.curr_piece = piece_3
        else:
            return Tetris.State.RotateBlocked
        return Tetris.State.Ok

    def move_left(self) -> State:
        new_pos = self.get_left_move()
        if self.overlap_check(self.curr_piece, new_pos):
            self.piece_pos = new_pos
        else:
            return Tetris.State.LeftBlocked
        return Tetris.State.Ok

    def move_down(self) -> State:
        if self.can_move_down():
            self.piece_pos = self.get_down_move()
        else:
            return Tetris.State.DownBlocked
        return Tetris.State.Ok

    def move_right(self) -> State:
        new_pos = self.get_right_move()
        if self.overlap_check(self.curr_piece, new_pos):
            self.piece_pos = new_pos
        else:
            return Tetris.State.RightBlocked
        return Tetris.State.Ok

    def get_left_move(self) -> tuple[int, int]:
        # Shift the piece left by 1 unit
        return (self.piece_pos[0], self.piece_pos[1] - 1)

    def get_right_move(self) -> tuple[int, int]:
        return (self.piece_pos[0], self.piece_pos[1] + 1)

    def get_down_move(self) -> tuple[int, int]:
        # Shift the piece down by 1 unit
        return (self.piece_pos[0] + 1, self.piece_pos[1])

    def can_move_down(self) -> bool:
        new_pos = self.get_down_move()
        return self.overlap_check(self.curr_piece, new_pos)

    def merge_board_and_piece(self) -> None:
        """
        Fixes the position of the passed piece at piece_pos in the board
        This means that the new piece will now come into the play

        We also remove any filled up rows from the board to continue the gameplay
        as it happends in a tetris game
        """
        curr_piece_size_x = len(self.curr_piece)
        curr_piece_size_y = len(self.curr_piece[0])
        for i in range(curr_piece_size_x):
            for j in range(curr_piece_size_y):
                self.board[self.piece_pos[0] + i][self.piece_pos[1] + j] = self.curr_piece[i][j] | self.board[self.piece_pos[0] + i][self.piece_pos[1] + j]

        # After merging the board and piece
        # If there are rows which are completely filled then remove those rows

        # Declare empty row to add later
        empty_row = [0] * self.EFF_BOARD_SIZE
        empty_row[0] = 1
        empty_row[self.EFF_BOARD_SIZE-1] = 1

        # Declare a constant row that is completely filled
        filled_row = [1] * self.EFF_BOARD_SIZE

        # Count the total filled rows in the board
        filled_rows = 0
        for row in self.board:
            if row == filled_row:
                filled_rows += 1

        # The last row is always a filled row because it is the boundary
        # So decrease the count for that one
        filled_rows -= 1

        for i in range(filled_rows):
            self.board.remove(filled_row)

        # Add extra empty rows on the top of the board to compensate for deleted rows
        for i in range(filled_rows):
            self.board.insert(0, empty_row)

    def display_state(self, error_message: str = '') -> str:
        output = Tetris.CLRSCR + "\n"

        board_copy = deepcopy(self.board)
        curr_piece_size_x = len(self.curr_piece)
        curr_piece_size_y = len(self.curr_piece[0])
        for i in range(curr_piece_size_x):
            for j in range(curr_piece_size_y):
                masked_val = self.curr_piece[i][j] | self.board[self.piece_pos[0] + i][self.piece_pos[1] + j]
                board_copy[self.piece_pos[0] + i][self.piece_pos[1] + j] = masked_val

        # Create the upper wall
        for i in range(self.EFF_BOARD_SIZE):
            output += "*"
        output += '\n'

        for i in range(self.EFF_BOARD_SIZE):
            for j in range(self.EFF_BOARD_SIZE):
                if board_copy[i][j] == 1:
                    output += "*"
                else:
                    output += " "
            output += '\n'

        if error_message:
            output += f"\n{error_message}\n"

        output += Tetris.INSTRUCTIONS
        return output

    def init_board(self) -> list[list[int]]:
        board = [[0 for x in range(self.EFF_BOARD_SIZE)] for y in range(self.EFF_BOARD_SIZE)]
        for i in range(self.EFF_BOARD_SIZE):
            board[i][0] = 1
        for i in range(self.EFF_BOARD_SIZE):
            board[self.EFF_BOARD_SIZE-1][i] = 1
        for i in range(self.EFF_BOARD_SIZE):
            board[i][self.EFF_BOARD_SIZE-1] = 1
        return board

    def get_random_piece(self) -> list[list[int]]:
        idx = random.randrange(len(Tetris.PIECES))
        return Tetris.PIECES[idx]

    def get_random_position(self) -> tuple[int, int]:
        curr_piece_size = len(self.curr_piece)

        # This x refers to rows, rows go along y-axis
        x = 0
        # This y refers to columns, columns go along x-axis
        y = random.randrange(1, self.EFF_BOARD_SIZE - curr_piece_size)
        return (x, y)
